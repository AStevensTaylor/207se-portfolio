#include <stdio.h>
#include <iostream>
#include <deque>

int main(int argc, char *argv[]) {
	std::deque<char> charDeque;
	
	std::cout << "Enter your name: ";
	char chr;
	
	while(chr != 10){
		chr = getchar();
		charDeque.push_back(chr);
	}
	
	int studentNumber;
	
	std::cout << "Enter your student ID: ";
	std::cin >> studentNumber;

	while(studentNumber >= 10){
		studentNumber /= 10;
	}

	int length = charDeque.size();
	
	for(int i = 0; i < length; i += studentNumber){
		for(int j = 0; j < studentNumber; j++){
			std::cout << charDeque.front();
			charDeque.pop_front();
		}
		std::cout << std::endl;
	}
}

