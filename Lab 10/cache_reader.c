#include "cache_reader.h"
#include <malloc.h>

//http://www.phim.unibe.ch/comp_doc/c_manual/C/SYNTAX/struct.html
//http://vergil.chemistry.gatech.edu/resources/programming/c-tutorial/structs.html


int refill(cr_file* buff){
  //Refills a buffer
  //Only works when completely used buffer
  if(buff->usedbuffer!=buff->bufferlength)
    return 0;
  else{
    buff->refillCount ++;
    buff->usedbuffer=0;
    int len=read(buff->file, buff->buffer, buff->bufferlength);
    //printf("Length read: %d\n", len);
    //If we didn't fill the buffer, fill up with EOF
    if(len<buff->bufferlength)
      for(int i=len;i<buff->bufferlength;i++)	
	buff->buffer[i]=EOF;  //Accessing like an array!
    return len;
  }
  
}

int cr_close(cr_file* f){
  printf("Buffer location 0x%016x\n", f->buffer);
//  free(f->buffer);
  close(f->file);
  return f->refillCount;
}


cr_file* cr_open(char * filename, int buffersize){

  if(buffersize<512)
  	buffersize=512;
  //Info on malloc
  //http://www.space.unibe.ch/comp_doc/c_manual/C/FUNCTIONS/malloc.html
  int f;
  if ((f = open(filename, O_DIRECT ,'r')) == -1){
    fprintf(stderr, "Cannot open %s\n", filename);
    return 0;
  }

  cr_file* a=(cr_file*)malloc(sizeof(cr_file));
  a->file=f;
  a->bufferlength=buffersize;
  a->usedbuffer=buffersize; //Start off with no characters, so refill will work as expected
  a->buffer =(char*)memalign(512, sizeof(char)*buffersize);

  printf("%s\n", a->buffer);
  refill(a);
  return a;
}

//------------------------------------------------------------------
char cr_read_byte(cr_file* f){
  // your code goes here
  if (refill(f));
  return f->buffer[f->usedbuffer ++];
  // remember that this needs to return a char (a byte, put another way..)
  //return EOF; // this is just so the compile works...
}



