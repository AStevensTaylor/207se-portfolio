#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <fcntl.h>

static char* liza [10] = {"Then mend it, dear Henry, dear Henry, dear Henry,\nThen mend it, dear Henry, dear Henry, mend it.",
			  "With a straw, dear Henry, dear Henry, dear Henry,\nWith a straw, dear Henry, dear Henry, with a straw.",
			  "Then cut it, dear Henry, dear Henry, dear Henry,\nThen cut it, dear Henry, dear Henry, cut it.",
			  "With a knife, dear Henry, dear Henry, dear Henry,\nWith a knife, dear Henry, dear Henry, with a knife.",
			  "Then sharpen it, dear Henry, dear Henry, dear Henry\nThen sharpen it, dear Henry, dear Henry, sharpen it.",
			  "On a stone, dear Henry, dear Henry, dear Henry,\nOn a stone, dear Henry, dear Henry, a stone.",
			  "Well wet it, dear Henry, dear Henry, dear Henry,\nWell wet it, dear Henry, dear Henry, wet it.",
			  "try water, dear Henry, dear Henry, dear Henry,\ntry water, dear Henry, dear Henry, water.",
			  "In a bucket, dear Henry, dear Henry, dear Henry,\nIn a bucket, dear Henry, dear Henry, a bucket.",
			  "Use your head, then! dear Henry, dear Henry, dear Henry,\nUse your head, then! dear Henry, dear Henry, use your head!"
};

static char* henry [10] = {"There's a hole in the bucket, dear Liza, dear Liza,\nThere's a hole in the bucket, dear Liza, a hole.",
			   "With what shall I mend it, dear Liza, dear Liza?\nWith what shall I mend it, dear Liza, with what?",
			   "The straw is too long, dear Liza, dear Liza,\nThe straw is too long, dear Liza, too long,",
			   "With what shall I cut it, dear Liza, dear Liza?\nWith what shall I cut it, dear Liza, with what?",
			   "The knife is too dull, dear Liza, dear Liza,\nThe knife is too dull, dear Liza, too dull.",
			   "On what shall I sharpen it, dear Liza, dear Liza?\nOn what shall I sharpen it, dear Liza, on what?",
			   "The stone is too dry, dear Liza, dear Liza,\nThe stone is too dry, dear Liza, too dry.",
			   "With what shall I wet it, dear Liza, dear Liza?\nWith what shall I wet it, dear Liza, with what?",
			   "In what shall I fetch it, dear Liza, dear Liza?\nIn what shall I fetch it, dear Liza, in what?",
			   "There's a hole in my bucket, dear Liza, dear Liza,\nThere's a hole in my bucket, dear Liza, a hole."
};


int main(){

  int id;
  sem_t *henry_sem = sem_open("h", O_CREAT, 0600, 1);
  sem_t *liza_sem = sem_open("l", O_CREAT, 0600, 0);

  sem_unlink("h");
  sem_unlink("l");

           
  int pid=fork();
  if(pid){
    //Process 1 :: Liza

    int i =0;
    while(i < 10){
      //Process 1 Loop :: Liza
      // 1 = other, 0 = self

      //wait (Apparently)
      sem_wait(liza_sem);

      fprintf(stderr, "%s\n\n", liza[i]);
      fflush(stderr);
      i++;
      
      //signal
      sem_post(henry_sem);
      //printf("Liza Finished\n\n");
      
    }
  }else{
    //Process 2 :: Henry

    int i = 0;
    while(i < 10){
      //Process 2 Loop :: Henry
      // 0 = other, 1 = self
      
            
      //wait (Apparently)
      sem_wait(henry_sem);
            
      //critical section
      fprintf(stdout, "%s\n", henry[i]);
      fflush(stdout);
      i++;

      //signal
      sem_post(liza_sem);
      
      //printf("Henry Finished\n\n");
      
    }
    
  }

  sem_destroy(liza_sem);
  sem_destroy(henry_sem);
  
}
