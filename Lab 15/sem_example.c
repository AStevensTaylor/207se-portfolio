//Modified from 
//http://docs.linux.cz/programming/c/unix_examples/semab.html

#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>

//#define PROCESSES 2

static char* liza [10] = {"Then mend it, dear Henry, dear Henry, dear Henry,\nThen mend it, dear Henry, dear Henry, mend it.", "With a straw, dear Henry, dear Henry, dear Henry,\nWith a straw, dear Henry, dear Henry, with a straw.", "Then cut it, dear Henry, dear Henry, dear Henry,\nThen cut it, dear Henry, dear Henry, cut it.", "With a knife, dear Henry, dear Henry, dear Henry,\nWith a knife, dear Henry, dear Henry, with a knife.", "Then sharpen it, dear Henry, dear Henry, dear Henry\nThen sharpen it, dear Henry, dear Henry, sharpen it.", "On a stone, dear Henry, dear Henry, dear Henry,\nOn a stone, dear Henry, dear Henry, a stone.", "Well wet it, dear Henry, dear Henry, dear Henry,\nWell wet it, dear Henry, dear Henry, wet it.", "try water, dear Henry, dear Henry, dear Henry,\ntry water, dear Henry, dear Henry, water.", "In a bucket, dear Henry, dear Henry, dear Henry,\nIn a bucket, dear Henry, dear Henry, a bucket.", "Use your head, then! dear Henry, dear Henry, dear Henry,\nUse your head, then! dear Henry, dear Henry, use your head!"
};

static char* henry [10] = {"There's a hole in the bucket, dear Liza, dear Liza,\nThere's a hole in the bucket, dear Liza, a hole.", "With what shall I mend it, dear Liza, dear Liza?\nWith what shall I mend it, dear Liza, with what?", "The straw is too long, dear Liza, dear Liza,\nThe straw is too long, dear Liza, too long,", "With what shall I cut it, dear Liza, dear Liza?\nWith what shall I cut it, dear Liza, with what?", "The knife is too dull, dear Liza, dear Liza,\nThe knife is too dull, dear Liza, too dull.", "On what shall I sharpen it, dear Liza, dear Liza?\nOn what shall I sharpen it, dear Liza, on what?", "The stone is too dry, dear Liza, dear Liza,\nThe stone is too dry, dear Liza, too dry.", "With what shall I wet it, dear Liza, dear Liza?\nWith what shall I wet it, dear Liza, with what?", "In what shall I fetch it, dear Liza, dear Liza?\nIn what shall I fetch it, dear Liza, in what?", "There's a hole in my bucket, dear Liza, dear Liza,\nThere's a hole in my bucket, dear Liza, a hole."
};


int main(){
  union semun {
    int val;
    struct semid_ds *buf;
    ushort * array;
  } argument;
  int id;
  int sem_key;
  sem_key = ftok("./sem_example.c", 'R');
            
  id = semget(sem_key, 2, 0600 | IPC_CREAT);
  if(id < 0)
    {
      perror("[P] semget");
      exit(1);
    }
 
  //Set initial values
  //Process 1 :: Liza = 0
  argument.val = 0;
  if( semctl(id, 0, SETVAL, argument) < 0)
    {
      perror("[L]: semctl");
      exit(1);
    }
  //Process 2 :: Henry = 1
  argument.val = 1;
  if ( semctl(id, 1, SETVAL, argument) < 0)
    {
      perror("[H]: semctl");
      exit(1);
    }
  int pid=fork();
  if(pid){
    //Process 1 :: Liza
    int retval;
    id = semget(sem_key, 2, 0600);
    if(id < 0){
      perror("[L] semget");
      exit(0);
    }

    struct sembuf henry_sem [1];
    henry_sem[0].sem_num = 1;
    henry_sem[0].sem_flg = 0;
    henry_sem[0].sem_op = 1; //Will always post

    struct sembuf liza_sem [1];
    liza_sem[0].sem_num = 0;
    liza_sem[0].sem_flg = 0;
    liza_sem[0].sem_op = -1; //Will always wait, and lock

    int i =0;
    while(i < 10){
      //Process 1 Loop :: Liza
      // 1 = other, 0 = self
      //wait (Apparently)
      retval = semop(id, liza_sem, 1);
      
      fprintf(stderr, "%s\n\n", liza[i]);
      fflush(stderr);
      i++;
      
      //signal  
      retval = semop(id, henry_sem, 1);
    }
  }else{
    //Process 2 :: Henry
    int retval;
    id = semget(sem_key, 2, 0600);
    if(id < 0){
      perror("[H] semget");
      exit(0);
    }

    struct sembuf henry_sem [1];
    henry_sem[0].sem_num = 1;
    henry_sem[0].sem_flg = 0;
    henry_sem[0].sem_op = -1; //Will always wait, and lock

    struct sembuf liza_sem [1];
    liza_sem[0].sem_num = 0;
    liza_sem[0].sem_flg = 0;
    liza_sem[0].sem_op = 1; //Will always post

    int i = 0;
    while(i < 10){
      //Process 2 Loop :: Henry
      // 0 = other, 1 = self
      //wait (Apparently)
      retval = semop(id, henry_sem, 1);
            
      //critical section
      fprintf(stdout, "%s\n", henry[i]);
      fflush(stdout);
      i++;

      //signal
      retval = semop(id, liza_sem, 1);
    }
  }
  semctl(id, 0, IPC_RMID);
}
