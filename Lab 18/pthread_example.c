/*Adapted from 
http://www.yolinux.com/TUTORIALS/LinuxTutorialPosixThreads.html

Compile with 
gcc -o pthread_example pthread_example.c -lpthread
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *square_numbers( void* ptr);

typedef struct Thread_Data
{
  int remainer_number;
  int* data;
  int start;
  int end;
} thread_data;

int main()
{
  printf("Number of items to process: ");
  int item_count;
  scanf("%d", &item_count);
  printf("Number of threads: ");
  int thread_count;
  scanf("%d", &thread_count);

  pthread_t thread [thread_count];
  thread_data threads [thread_count];

  int remainingNumbers = item_count % thread_count;

  int items [item_count];
  for(int i = 0; i < item_count; i++)
    {
      items[i] = rand()%50+2;
    }

  for(int i = 0; i < thread_count; i ++)
    {
      threads[i].start = i*(item_count/thread_count);
      threads[i].end = (i+1) * (item_count/thread_count) - 1;
      threads[i].data = items;
      if(remainingNumbers > 0)
	{
	  threads[i].remainer_number = items[item_count - 1];
	  item_count --;
	  remainingNumbers --;
	}
      else
	{
	  threads[i].remainer_number = 0;
	}
    }


  for(int i = 0; i < thread_count; i++)
      pthread_create(&thread[i], NULL, square_numbers,(void*) &threads[i]);

  for(int i = 0; i < thread_count; i++)
    pthread_join( thread[i], NULL);
  
  for(int i = 0; i < thread_count; i++)
    {
      printf("Thread %d returned:\n", i);
      for(int j = threads[i].start; j <= threads[i].end; j++)
	{
	  printf("\t- %d\n", threads[i].data[j]);
	}
      if(threads[i].remainer_number != 0)
	{
	  printf("\t- %d\n", threads[i].remainer_number);
	}
    }

  exit(0);
}

void *square_numbers( void* ptr )
{

  thread_data *t_data = (thread_data*) ptr;
  
  for(int i = t_data->start; i <= t_data->end; i ++)
    t_data->data[i] = t_data->data[i] * t_data->data[i];

  if(t_data->remainer_number > 0)
    t_data->remainer_number = t_data->remainer_number * t_data->remainer_number;
}
