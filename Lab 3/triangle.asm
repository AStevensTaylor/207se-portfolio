section .data
	global _start
	char db ' '
	number_len	equ 1
	string db 'Enter your number (0-9): '
	string_len equ $-string

section .bss
	number resb number_len
		
section .text

_start:
	call pr_str
	call get_input
	;; use ax for the row counter
	mov rax, [number]
	sub rax, 30H
	
outer_loop:
	;; use bx for the column counter
	mov rbx, [number]
	sub rbx, 30H

inner_loop:
	;; draw a dot
	call dot

	;; subtract from the counter
	dec rbx
	cmp rbx,rax
	jge inner_loop

	call linefeed

	;; subtract from the counter
	;; and repeat if we haven't hit 0	
	dec ax
	cmp ax,0
	jne outer_loop

	call exit

;; Some useful routines that I can call many times
;; or even copy and paste into other code
	
exit:	
	mov rax,1            
	mov rbx,0            
	int 80h
	ret

linefeed:	
	
	mov [char],  byte 10
	call pr_char

	ret
	
dot:	

	mov [char],  byte '.'
	call pr_char

	ret

pr_char:	
	push rax
	push rbx
	push rcx
	push rdx

	mov rax,4            
	mov rbx,1
	mov rcx, char
	mov rdx,1
	int 80h              

	pop rdx
	pop rcx
	pop rbx
	pop rax

	ret

pr_str:
	push rax
	push rbx
	push rcx
	push rdx

	mov rax,4            
	mov rbx,1
	mov rcx, string
	mov rdx, string_len
	int 80h              

	pop rdx
	pop rcx
	pop rbx
	pop rax

	ret

get_input:
	push rax
	push rbx
	push rcx
	push rdx

	mov rax, 3           
	mov rbx, 0
	mov rcx, number
	mov rdx, number_len
	int 80h            

	pop rdx
	pop rcx
	pop rbx
	pop rax

	ret
