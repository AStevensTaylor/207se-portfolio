[BITS 16]
[ORG 0x7C00]     
  

top:
        ;; Put 0 into ds (data segment)
        ;; Can't do it directly
        mov ax,0x0000
        mov ds,ax

        mov si, Query
	call write_string
	call line
	mov ch, dl
outer_loop:
	mov cl, dl
inner_loop:
	call dot
	dec cl
	cmp cl, ch
	jge inner_loop
	
	call line
	
	dec ch
	cmp ch, 0
	jne outer_loop		

        jmp $           ; Spin
line:
	mov al, 0x0D
	call write_char
	mov al, 0x0A
	call write_char
	ret
dot:
	mov al, '.'

write_char:
        mov ah,0x0E     ; Display a chacter (as before)
        mov bh,0x00     
        mov bl,0x07     
        int 0x10        ; BIOS interrupt
	ret 

get_input:
	mov ah, 0x00
	int 0x16
	sub al, 0x30
	mov dl, al
	ret

write_string:
        mov ah,0x0E     ; Display a chacter (as before)
        mov bh,0x00     
        mov bl,0x07     
nextchar:       
        lodsb   ; Loads [SI] into AL and increases SI by one
        cmp al,0                ; End of the string?
        jz get_input
        int 0x10        ; BIOS interrupt 
        jmp nextchar    

Query db 'Enter a number (0-9): ',0 ; Null-terminated 
times 510-($-$$) db 0  
dw 0xAA55 
