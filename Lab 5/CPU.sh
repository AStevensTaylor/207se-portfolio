#!/bin/bash

while $true
do
echo "Welcome to CPU.sh
1) List CPU info
2) List Interrupts
3) List processes
4) Quit

"
read input

case $input in
1)
cat /proc/cpuinfo
;;
2)
cat /proc/interrupts
;;
3)
echo "Enter a process name: "
read pname
ps -A | grep $pname | awk '{print $pname, $1, $2}'
;;
4)
break
;;
esac
read
clear
done
