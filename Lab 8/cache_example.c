#import "cache_reader.h"

//Simple file display to show how easy it is to use the cached reader functions

int main(){
  char c;

  //Open a file
  cr_file* f = cr_open("text",20);  
  
  int count = 0;

  //While there are useful bytes coming from it
  while((c=cr_read_byte(f))!=EOF)
  {
    //Print them
    printf("%c",c);
    count ++;
  }
  
  printf("Bytes counted %d\n", count);
  //Then close the file
  int buffer = cr_close(f);

  printf("Buffer refilled %d\nY", buffer);

  //And finish
  return 0;
}
